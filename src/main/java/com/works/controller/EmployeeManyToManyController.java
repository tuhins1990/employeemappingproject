package com.works.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.works.dto.EmployeeDto;
import com.works.service.EmployeeService;

@RestController
public class EmployeeManyToManyController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/employees")
	public ResponseEntity<List<EmployeeDto>> getAllEmployees() {
		List<EmployeeDto> employees = employeeService.getAllEmployees();
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	@PostMapping("/save/employee")
	public ResponseEntity<EmployeeDto> saveAllEmployees(@RequestBody EmployeeDto employeeDto) {
		EmployeeDto std = employeeService.addEmployee(employeeDto);
		return new ResponseEntity<>(std, HttpStatus.CREATED);
	}

	@PutMapping("/employee/{id}")
	public ResponseEntity<EmployeeDto> updateEmployees(@PathVariable(name = "id") Integer id,
			@RequestBody EmployeeDto employee) {
		EmployeeDto employeeDto = employeeService.updateEmployee(id, employee);
		return new ResponseEntity<>(employeeDto, HttpStatus.CREATED);
	}

	@DeleteMapping("/employee/{id}")
	public ResponseEntity<String> deleteEmployees(@PathVariable(name = "id") Integer employeeId) {
		String message = employeeService.deleteEmployees(employeeId);
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}
