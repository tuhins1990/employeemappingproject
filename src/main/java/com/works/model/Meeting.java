package com.works.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "MEETING")
public class Meeting {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "meeting_sequence")
	@SequenceGenerator(name = "meeting_sequence", sequenceName = "meeting_sequence")
	private Integer id;

	@Column(name = "meetingName")
	private String meetingName;

	@ManyToMany(mappedBy = "meetings")
	@JsonIgnore
	private Set<Employee> employees;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public String getMeetingName() {
		return meetingName;
	}

	public void setMeetingName(String meetingName) {
		this.meetingName = meetingName;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	
	
	
}
