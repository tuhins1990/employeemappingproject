package com.works.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.works.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
