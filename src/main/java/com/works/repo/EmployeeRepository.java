package com.works.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.works.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
