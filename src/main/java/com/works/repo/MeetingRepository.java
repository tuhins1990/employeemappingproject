package com.works.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.works.model.Meeting;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting, Integer> {

	public Meeting findByMeetingName(String meetingName);
}
