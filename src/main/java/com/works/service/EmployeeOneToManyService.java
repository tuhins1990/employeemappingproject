package com.works.service;

import java.util.List;

import com.works.model.Employee;

public interface EmployeeOneToManyService {
	public Employee addEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public Employee editEmployees(Employee employee);

	public void deleteEmployees(Integer employeeId);

}
