package com.works.service;

import java.util.List;

import com.works.dto.EmployeeDto;

public interface EmployeeService {

	public EmployeeDto addEmployee(EmployeeDto employeeDto);

	public List<EmployeeDto> getAllEmployees();

	public EmployeeDto updateEmployee(Integer employeeId, EmployeeDto employee);

	public String deleteEmployees(Integer employeeId);
}
