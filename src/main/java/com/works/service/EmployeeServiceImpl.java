package com.works.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.works.dto.EmployeeDto;
import com.works.model.Employee;
import com.works.model.Meeting;
import com.works.repo.EmployeeRepository;
import com.works.repo.MeetingRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Resource
	private EmployeeRepository employeeRepository;

	@Resource
	private MeetingRepository meetingRepository;
	
	

	@Transactional
	@Override
	public EmployeeDto addEmployee(EmployeeDto employeeDto) {
		Employee employee = new Employee();
		mapDtoToEntity(employeeDto, employee);
		Employee savedEmployee = employeeRepository.save(employee);
		return mapEntityToDto(savedEmployee);
	}

	@Override
	public List<EmployeeDto> getAllEmployees() {
		List<EmployeeDto> employeeDtos = new ArrayList<>();
		List<Employee> employees = employeeRepository.findAll();
		employees.stream().forEach(employee -> {
			EmployeeDto employeeDto = mapEntityToDto(employee);
			employeeDtos.add(employeeDto);
		});
		return employeeDtos;
	}

	@Transactional
	@Override
	public EmployeeDto updateEmployee(Integer id, EmployeeDto employeeDto) {
		Employee employee = employeeRepository.getOne(id);
		employee.getMeetings().clear();
		mapDtoToEntity(employeeDto, employee);
		Employee employeeRepo = employeeRepository.save(employee);
		return mapEntityToDto(employeeRepo);
	}

	@Override
	public String deleteEmployees(Integer emploeeId) {
		
		Employee employee = employeeRepository.getOne(emploeeId);
		employee.removeMeetings();
		employeeRepository.deleteById(emploeeId);
		return "Employee with id: " + emploeeId + " deleted successfully!";
	}

	private void mapDtoToEntity(EmployeeDto employeeDto, Employee employee) {
		employee.setFirstName(employeeDto.getFirstName());
		employee.setLastName(employeeDto.getLastName());
		employee.setAddress(employeeDto.getAddress());
		employee.setSalary(employeeDto.getSalary());
		
		if (null == employee.getMeetings()) {
			employee.setMeetings(new HashSet<>());
		}
		employeeDto.getMeetings().stream().forEach(meetingName -> {
			Meeting meeting = meetingRepository.findByMeetingName(meetingName);
			if (null == meeting) {
				meeting = new Meeting();
				meeting.setEmployees(new HashSet<>());
			}
			meeting.setMeetingName(meetingName);
			employee.addMeeting(meeting);
		});
		
		
	}

	private EmployeeDto mapEntityToDto(Employee employee) {
		EmployeeDto responseDto = new EmployeeDto();
		responseDto.setFirstName(employee.getFirstName());
		responseDto.setLastName(employee.getLastName());
		responseDto.setAddress(employee.getAddress());
		responseDto.setSalary(employee.getSalary());
		responseDto.setId(employee.getId());
		responseDto.setMeetings(employee.getMeetings().stream().map(Meeting::getMeetingName).collect(Collectors.toSet()));
		//responseDto.setId(employee.getId());
		return responseDto;
	}
}
